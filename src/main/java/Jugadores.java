public class Jugadores{
    public int id;
    public String nombre;
    public int voto;

    public Jugadores() {
    }

    public Jugadores(String nombre, int voto) {
        
        this.nombre = nombre;
        this.voto = voto;
    }

    public int getId() {
        return id; 
    }

    public String getNombre() {
        return nombre; 
    }

    public int getVoto() {
        return voto; 
    }

    public void setId(int id) {
        this.id = id; 
    }

    public void setNombre(String nombre) {
        this.nombre = nombre; 
    }

    public void setVoto(int voto) {
        this.voto = voto;
    }
}