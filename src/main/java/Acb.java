
import java.io.*;
import java.util.*;

import javax.servlet.*;
import javax.servlet.http.*;

public class Acb extends HttpServlet {

    private ModeloDatos bd;

    public void init(ServletConfig cfg) throws ServletException {
        bd = new ModeloDatos();
        bd.abrirConexion();
    }

    public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        HttpSession s = req.getSession(true);
        String nombreP = (String) req.getParameter("txtNombre");
        String nombre = (String) req.getParameter("R1");
        //String voto = (String) req.getParameter("B3");
        if(req.getParameter("B3") != null){
            bd.VotosaCero();
            res.sendRedirect(res.encodeRedirectURL("index.html"));
        }else if (req.getParameter("B4")!= null){
            Map<String, Integer> votos = new HashMap<String,Integer>();
            votos = bd.vervotos();
            req.setAttribute("votos", votos);
            res.sendRedirect(res.encodeRedirectURL("vervotos.jsp"));
        }else{
        
            if (nombre.equals("Otros")) {
                nombre = (String) req.getParameter("txtOtros");
                bd.insertarJugador(nombre);
            }
            if (bd.existeJugador(nombre)) {
                bd.actualizarJugador(nombre);
             } else {
               bd.insertarJugador(nombre);
            }
            s.setAttribute("nombreCliente", nombreP);
            // Llamada a la página jsp que nos da las gracias
           res.sendRedirect(res.encodeRedirectURL("TablaVotos.jsp"));
        }
    }
    public void destroy() {
        bd.cerrarConexion();
        super.destroy();
    }
}
