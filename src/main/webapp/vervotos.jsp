<%@ page import="java.util.*" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.io.*" %>
<html>
    <head>
        <link href="estilos.css" rel="stylesheet" type="text/css" />
        <title>Recuento de Votos de los Jugadores ABC</title>
    </head>
    <body class="resultado">
    <hr>
       <table border="1" width="100%">
        <tr>
            <th scope="col">Jugador</th>
            <th scope="col">Votos</th>
        </tr>
        <% 
            Map<String,Integer> votos = (Map<String,Integer>) session.getAttribute("votos");
            if (votos != null){
                for (Map.Entry<String,Integer> entry : votos.entrySet())
                { 
                    //String nombre= (String) entry.getKey(); 
                    //Integer voto= (Integer) entry.getValue();
                    System.out.println(" <tr>");
                    System.out.println(" <td> "+ entry.getKey()+"</td>");
                    System.out.println(" <td> "+ entry.getValue()+"</td>");
                    System.out.println(" </tr>");
                    }
            }else{
                System.out.println(" <tr>");
                    System.out.println(" <td> No hay datos que mostrar</td>");
                    System.out.println(" </tr>");
            }
        %>
        </table>
        <br> <a href="index.html"> Ir al comienzo</a>
    </body>
</html>